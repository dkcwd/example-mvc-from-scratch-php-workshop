<?php

namespace MyApp\Feature;

trait MapperAwareTrait
{

    protected $mapper;

    /**
     * @return mixed
     */
    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * @param mixed $mapper
     */
    public function setMapper($mapper)
    {
        $this->mapper = $mapper;
    }

}