<?php

namespace MyApp\Feature;

interface ConfigAwareInterface
{

    /**
     * @return mixed
     */
    public function getConfig();

    /**
     * @param mixed $config
     */
    public function setConfig($config);

}