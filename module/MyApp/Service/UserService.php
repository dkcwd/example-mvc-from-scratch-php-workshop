<?php

namespace MyApp\Service;

use Exception\Http\MethodNotAllowedException;
use MyApp\Entity\Collection\UserCollection;
use MyApp\Entity\User;
use MyApp\Feature\MapperAwareTrait;

class UserService extends AbstractService
{

    use MapperAwareTrait;

    public function create($user)
    {
        /**
         * @todo Logic to create a user
         */
        throw new MethodNotAllowedException('The create method is not allowed');
    }

    public function findOne($user)
    {
        /**
         * @todo Logic to return a user
         */
        return $user;
    }

    public function findAll($criteria)
    {
        /**
         * @todo Logic to return a user collection
         */
        $userCollection = new UserCollection();

        $user1 = new User();
        $user1->setUserId(5465321321);
        $user1->setFirstName('Jeff');
        $user1->setLastName('Thomson');
        $userCollection->add($user1);

        $user2 = new User();
        $user2->setUserId(5465321325);
        $user2->setFirstName('Lonnie');
        $user2->setLastName('Muchacho');
        $userCollection->add($user2);

        return $userCollection;
    }

    public function update($user)
    {
        /**
         * @todo Logic to update a user
         */
        throw new MethodNotAllowedException('The update method is not allowed');
    }

    public function delete($user)
    {
        /**
         * @todo Logic to delete a user
         */
        throw new MethodNotAllowedException('The delete method is not allowed');
    }

}