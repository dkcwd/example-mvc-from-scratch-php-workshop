<?php

namespace MyApp\Service;

use Exception\Http\MethodNotAllowedException;

abstract class AbstractService
{

    public function create($user)
    {
        // placeholder
        throw new MethodNotAllowedException('The create method is not allowed');
    }

    public function findOne($user)
    {
        // placeholder
        throw new MethodNotAllowedException('The read method is not allowed');
    }

    public function findAll($criteria)
    {
        // placeholder
        throw new MethodNotAllowedException('The read method is not allowed');
    }

    public function update($user)
    {
        // placeholder
        throw new MethodNotAllowedException('The update method is not allowed');
    }

    public function delete($user)
    {
        // placeholder
        throw new MethodNotAllowedException('The delete method is not allowed');
    }

}