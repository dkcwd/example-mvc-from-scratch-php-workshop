<?php

namespace MyApp\Controller;

use Exception\Http\MethodNotAllowedException;
use Mvc\Controller\MvcController;

abstract class CrudController extends MvcController
{

    public function action()
    {
        switch(strtolower($this->getRequest()->getMethod())) {
            case 'post': return $this->create();
            case 'get': return $this->read();
            case 'put': return $this->update();
            case 'delete': return $this->delete();
        }

        throw new MethodNotAllowedException('That method is not allowed');
    }

    public function create()
    {
        throw new MethodNotAllowedException('The create method is not allowed');
    }

    public function read()
    {
        throw new MethodNotAllowedException('The read method is not allowed');
    }

    public function update()
    {
        throw new MethodNotAllowedException('The update method is not allowed');
    }

    public function delete()
    {
        throw new MethodNotAllowedException('The delete method is not allowed');
    }

}