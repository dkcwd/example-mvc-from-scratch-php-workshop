<?php

namespace MyApp\Controller\Resource\Factory;

use MyApp\Controller\Resource\User;
use Mvc\Controller\Factory\FactoryInterface;
use MyApp\Feature\ConfigAwareTrait;
use MyApp\Feature\ConfigAwareInterface;
use MyApp\Mapper\UserMapper;
use MyApp\Service\UserService;

class UserFactory implements FactoryInterface, ConfigAwareInterface
{

    use ConfigAwareTrait;

    public function make($name = null)
    {
        $controller = new User();
        $service = new UserService($this->getConfig());
        $mapper = new UserMapper($this->getConfig());
        $service->setMapper($mapper);
        $controller->setService($service);
        return $controller;
    }

}