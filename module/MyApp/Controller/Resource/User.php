<?php

namespace MyApp\Controller\Resource;

use MyApp\Controller\CrudController;
use MyApp\Feature\ServiceAwareTrait;

class User extends CrudController
{

    use ServiceAwareTrait;

    public function create()
    {
        /**
         * filter and validate post data, then persist
         */
        $user = new \MyApp\Entity\User();
        $user = $this->getService()->create($user);
        // $data = array("user_id" => "65465456-465464-5454-56-4561", "name" => $_POST['name']);
        return $user->toArray();
    }

    public function read()
    {
        /**
         * Do check for read one or read many
         */

//        // if one
//        $user = new \MyApp\Entity\User();
//        $user->setUserId(132132564654);
//        $user = $this->getService()->findOne($user);
//        return $user->toArray();

        // if many
        $criteria = new \MyApp\Entity\Search\Criteria();
        $userCollection = $this->getService()->findAll($criteria);
        return $userCollection->toArray();

    }

}