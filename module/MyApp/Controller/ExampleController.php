<?php

namespace MyApp\Controller;

use Mvc\Controller\MvcController;

class ExampleController extends MvcController
{

//    public static function init()
//    {
//        // placeholder
//        echo 'After controller has been built and configured';
//    }
//
//    public static function pre()
//    {
//        // placeholder
//        echo 'After the init() method';
//    }

    public function action()
    {
        return array(
            array("todo" => "Go shopping", "read" => true),
            array("todo" => "Make dinner", "read" => false)
        );
    }

//    public static function post()
//    {
//        // placeholder
//        echo 'After the action() method';
//    }

}