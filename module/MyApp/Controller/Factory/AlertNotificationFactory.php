<?php

namespace MyApp\Controller\Factory;

use MyApp\Controller\AlertNotificationController;
use Mvc\Controller\Factory\FactoryInterface;
use MyApp\Feature\ConfigAwareTrait;
use MyApp\Feature\ConfigAwareInterface;

class AlertNotificationFactory implements FactoryInterface, ConfigAwareInterface
{

    use ConfigAwareTrait;

    public function make($name = null)
    {
        $controller = new AlertNotificationController();
        return $controller;
    }

}