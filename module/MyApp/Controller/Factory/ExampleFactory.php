<?php

namespace MyApp\Controller\Factory;

use MyApp\Controller\ExampleController;
use Mvc\Controller\Factory\FactoryInterface;
use MyApp\Feature\ConfigAwareTrait;
use MyApp\Feature\ConfigAwareInterface;

class ExampleFactory implements FactoryInterface, ConfigAwareInterface
{

    use ConfigAwareTrait;

    public function make($name = null)
    {
        $controller = new ExampleController();
        return $controller;
    }

}