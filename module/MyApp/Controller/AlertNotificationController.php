<?php

namespace MyApp\Controller;

use Mvc\Controller\MvcController;

class AlertNotificationController extends MvcController
{

    public function action()
    {
        return array(
            'type' => 'success',
            'data' => 'Wow what a wonderful trick!',
        );
    }

}