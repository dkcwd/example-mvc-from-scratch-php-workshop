<?php

namespace MyApp\Entity\Collection;

abstract class AbstractEntityCollection
{

    protected $entities;

    public function toArray()
    {
        $array = array();

        foreach ($this->entities as $entity) {
            $array[] = $entity->toArray();
        }

        return $array;
    }

    public function add($entity)
    {
        $this->entities[] = $entity;
    }

}