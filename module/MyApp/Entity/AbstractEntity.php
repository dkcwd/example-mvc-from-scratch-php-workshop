<?php

namespace MyApp\Entity;

abstract class AbstractEntity
{

    public function toArray()
    {
        $array = array();

        foreach (get_class_vars(get_class($this)) as $key => $value) {
            $method = 'get' . ucfirst($key);
            $array[$key] = $this->$method();
        }

        return $array;
    }

    public function fromArray($array)
    {
        if (! is_array($array)) {
            throw new \RuntimeException(
                sprintf(
                    "Expected an array to be provided, received %s instead",
                    gettype($array)
                )
            );
        }

        foreach (get_class_vars(get_class($this)) as $key => $value) {
            $method = 'set' . ucfirst($key);
            $this->$method($array[$key]);
        }

        return $this;
    }


}