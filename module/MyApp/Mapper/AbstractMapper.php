<?php

namespace MyApp\Mapper;

use Exception\Http\MethodNotAllowedException;

abstract class AbstractMapper
{

    public function create()
    {
        throw new MethodNotAllowedException('The create method is not allowed');
    }

    public function findOne()
    {
        throw new MethodNotAllowedException('The read method is not allowed');
    }

    public function findAll()
    {
        throw new MethodNotAllowedException('The update method is not allowed');
    }

    public function delete()
    {
        throw new MethodNotAllowedException('The delete method is not allowed');
    }

}