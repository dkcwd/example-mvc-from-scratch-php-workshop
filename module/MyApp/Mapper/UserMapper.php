<?php

namespace MyApp\Mapper;

use Exception\Http\MethodNotAllowedException;

class UserMapper extends AbstractMapper
{

    public function create($user)
    {
        throw new MethodNotAllowedException('The create method is not allowed');
    }

    public function findOne($user)
    {
        throw new MethodNotAllowedException('The read method is not allowed');
    }

    public function findAll($criteria)
    {
        throw new MethodNotAllowedException('The update method is not allowed');
    }

    public function update($user)
    {
        throw new MethodNotAllowedException('The create method is not allowed');
    }

    public function delete($user)
    {
        throw new MethodNotAllowedException('The delete method is not allowed');
    }

}