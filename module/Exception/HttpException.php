<?php

namespace Exception;

class HttpException extends \Exception
{

    protected $code = 500;
    protected $header = 'HTTP/1.0 500 INTERNAL SERVER ERROR';

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

}