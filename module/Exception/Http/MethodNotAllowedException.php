<?php

namespace Exception\Http;

use Exception\HttpException;

class MethodNotAllowedException extends HttpException
{

    protected $code = 405;
    protected $header = 'HTTP/1.0 405 METHOD NOT ALLOWED';

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

}