<?php

namespace Exception\Http;

use Exception\HttpException;

class ForbiddenException extends HttpException
{

    protected $code = 403;
    protected $header = 'HTTP/1.0 403 FORBIDDEN';

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

}