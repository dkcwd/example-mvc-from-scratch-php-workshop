<?php

namespace Exception\Http;

use Exception\HttpException;

class BadRequestException extends HttpException
{

    protected $code = 400;
    protected $header = 'HTTP/1.0 400 BAD REQUEST';

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

}