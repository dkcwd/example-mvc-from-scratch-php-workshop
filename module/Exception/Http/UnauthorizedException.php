<?php

namespace Exception\Http;

use Exception\HttpException;

class UnauthorizedException extends HttpException
{

    protected $code = 401;
    protected $header = 'HTTP/1.0 401 UNAUTHORIZED';

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

}