<?php

namespace Exception\Http;

use Exception\HttpException;

class NotFoundException extends HttpException
{

    protected $code = 404;
    protected $header = 'HTTP/1.0 404 NOT FOUND';

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

}