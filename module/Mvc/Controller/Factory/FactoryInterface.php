<?php

namespace Mvc\Controller\Factory;

interface FactoryInterface
{
    
    /**
     * @param string|null $name The name to identify what should be created if required
     * @return mixed
     */
    public function make($name = null);

}