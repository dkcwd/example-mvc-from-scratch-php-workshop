<?php

namespace Mvc\Controller\Factory;

use Exception\HttpException;
use MyApp\Feature\ConfigAwareTrait;
use MyApp\Feature\ConfigAwareInterface;

class ControllerFactory implements FactoryInterface, ConfigAwareInterface
{

    use ConfigAwareTrait;

    /**
     * @param null $name
     * @return mixed
     * @throws HttpException
     */
    public function make($name = null)
    {

        $config = $this->getConfig();

        if (! isset($config['controllers'])) {
            throw new HttpException('A configuration error has been encountered');
        }

        if (! isset($config['controllers'][$name])) {
            throw new HttpException('A configuration error has been encountered');
        }

        if (! class_exists($config['controllers'][$name])) {
            throw new HttpException('A configuration error has been encountered');
        }

        $controllerFactory = new $config['controllers'][$name]();

        if (! $controllerFactory instanceof FactoryInterface) {
            // The controller factory is very opinionated....expects every controller to have a factory
            throw new HttpException('A configuration error has been encountered');
        }

        $controllerFactory->setConfig($config);
        return $controllerFactory->make();
    }

}