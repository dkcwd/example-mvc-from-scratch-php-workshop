<?php

namespace Mvc\Controller;

abstract class MvcController
{

    protected $request;

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    public static function init()
    {
        // placeholder
    }

    public static function pre()
    {
        // placeholder
    }

    public function action()
    {
        return array();
    }

    public static function post()
    {
        // placeholder
    }
}