<?php

namespace Mvc\App\Request;

class Request
{

    protected $method = 'get';
    protected $whiteList = array();
    protected $data = array();

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getWhiteList()
    {
        return $this->whiteList;
    }

    /**
     * @param array $whiteList
     */
    public function setWhiteList($whiteList)
    {
        $this->whiteList = $whiteList;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $request
     */
    public function setData($request)
    {
        foreach($request as $key => $value) {
            if (! isset($this->whiteList[strtolower($key)])) {
                continue;
            }

            if (strtolower($key) == 'request_method') {
                $this->setMethod(strtolower($value));
            }

            $this->data[strtolower($key)] = $value;
        }
    }

}