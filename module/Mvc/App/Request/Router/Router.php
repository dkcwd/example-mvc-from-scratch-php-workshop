<?php

namespace Mvc\App\Request\Router;

use Exception\Http\NotFoundException;
use Mvc\App\Request\Request;

class Router
{

    protected $routes = array();

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param array $routes
     */
    public function setRoutes($routes)
    {
        $this->routes = $routes;
    }

    public function resolve(Request $request)
    {
        $requestData = $request->getData();
        $requestUri = $requestData['request_uri'];
        $uri = explode('?', $requestUri);

        if (! isset($this->routes[strtolower($uri[0])])) {
            throw new NotFoundException('The resource cannot be found');
        }

        if (! isset($this->routes[strtolower($uri[0])]['controller'])) {
            throw new NotFoundException('A configuration error has been encountered');
        }

        return $this->routes[strtolower($uri[0])]['controller'];
    }

}