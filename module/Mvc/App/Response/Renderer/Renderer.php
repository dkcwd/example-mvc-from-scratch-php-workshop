<?php

namespace Mvc\App\Response\Renderer;

class Renderer
{

    protected $strategy;

    /**
     * @param mixed $strategy
     */
    public function setStrategy($strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @return mixed
     */
    public function getStrategy()
    {
        return $this->strategy;
    }

    /**
     * @param $response
     * @return mixed
     */
    public function render($response)
    {
        return $this->getStrategy()->render($response);
    }

}