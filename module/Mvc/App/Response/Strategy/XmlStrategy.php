<?php

namespace Mvc\App\Response\Strategy;

use \Mvc\App\Response\Response;

class XmlStrategy
{

    /**
     * @param $response
     * @return mixed
     */
    public function render(Response $response)
    {
        $response->addHeader("Content-Type: application/xml");
        $xml = new \SimpleXMLElement('<data/>');
        $this->toXml($xml, $response->getContent());
        return $xml->asXml();
    }

    /**
     * @param \SimpleXMLElement $object
     * @param array $data
     */
    public function toXml(\SimpleXMLElement $object, array $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (is_numeric($key)) {
                    $key = 'datum';
                }
                $newObject = $object->addChild($key);
                $this->toXml($newObject, $value);
            } else {
                $object->addChild($key, $value);
            }
        }
    }

}