<?php

namespace Mvc\App\Response\Strategy;

use Mvc\App\Response\Response;

class JsonStrategy
{

    public function render(Response $response)
    {
        $response->addHeader("Content-Type: application/json");
        return json_encode($response->getContent());
    }

}