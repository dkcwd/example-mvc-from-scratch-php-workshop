<?php

namespace Mvc\App\Response;

class Response
{

    protected $headers;
    protected $content;

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return mixed
     */
    public function addHeader($header)
    {
        $headers = $this->getHeaders();
        $headers[] = $header;
        $this->setHeaders($headers);
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Output the response
     */
    public function send()
    {
        foreach($this->getHeaders() as $header) {
            header($header);
        }
        echo $this->getContent();
    }

}