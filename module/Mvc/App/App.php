<?php

namespace Mvc\App;

use Exception\Http\BadRequestException;
use Exception\HttpException;
use Mvc\App\Request\Request;
use Mvc\App\Request\Router\Router;
use Mvc\App\Response\Renderer\Renderer;
use Mvc\App\Response\Response;
use Mvc\App\Response\Strategy\JsonStrategy;
use Mvc\App\Response\Strategy\XmlStrategy;
use Mvc\Controller\Factory\ControllerFactory;

class App
{

    protected $config;
    protected $controller;
    protected $renderer;
    protected $response;
    protected $request;
    protected $router;

    public static function init($config = array())
    {
        $app = new App();
        $app->config = $config;
        return $app;
    }

    public function run()
    {
        try {

            /**
             * Build request
             */
            $this->request = new Request();
            $this->request->setWhiteList(array_flip($this->config['request_data_whitelist']));
            $this->request->setData($_SERVER);

            /**
             * Resolve route to controller
             */
            $this->router = new Router();
            $this->router->setRoutes($this->config['routes']);
            $controllerName = $this->router->resolve($this->request);

            /**
             * Instantiate and configure controller via factory
             */
            $controllerFactory = new ControllerFactory();
            $controllerFactory->setConfig($this->config);
            $this->controller = $controllerFactory->make($controllerName);
            $this->controller->setRequest($this->request);

            /**
             * Call controller hooks
             */
            $this->controller->init();
            $this->controller->pre();
            $data = $this->controller->action();
            $this->controller->post();

            /**
             * Build response
             */
            $this->response = new Response();
            $this->response->setContent($data);

            /**
             * Set the response content rendering strategy
             */
            $this->renderer = new Renderer();
            $this->renderer->setStrategy(new JsonStrategy());

            /**
             * Format response content
             */
            $this->response->setContent($this->renderer->render($this->response));

            /**
             * Send response
             */
            $this->response->send();

        } catch (HttpException $e) {

            /**
             * Handle HttpExceptions
             */
            $this->response = new Response();

            $this->renderer = new Renderer();
            $this->renderer->setStrategy(new JsonStrategy());
            $this->response->addHeader($e->getHeader());
            $this->response->setContent(array('status' => $e->getCode(), 'message' => $e->getMessage()));
            $this->response->setContent($this->renderer->render($this->response));
            $this->response->send();

        }

    }

}