<?php

if (! stream_resolve_include_path('vendor/autoload.php')) {
    throw new RuntimeException('No autoload file could be found');
}

include 'vendor/autoload.php';