# Example application

## Installation
*Note: requires composer and php >= 5.4 (unless you want to set up a virtual host - PHP has it's own web server for quick prototyping like this since 5.4)*

1. Checkout this repository then open a terminal and switch into the repository then run composer update

2. Run PHP's internal webserver, in a console switch into the public directory and then type

```
php -S localhost:9412
```

You should now be able to visit localhost:9412 in your browser, note you can use an alternative port if you like




