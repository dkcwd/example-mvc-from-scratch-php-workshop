<?php

namespace MyAppTestAssets\Entity;

use MyApp\Entity\AbstractEntity;

class ExampleEntity extends AbstractEntity
{

    /** @var string */
    protected $propertyName1;

    /** @var int */
    protected $propertyName2;

    /** @var float */
    protected $propertyName3;

    /** @var array */
    protected $propertyName4;

    /** @var \StdClass */
    protected $propertyName5;

    /**
     * @return string
     */
    public function getPropertyName1()
    {
        return $this->propertyName1;
    }

    /**
     * @param string $propertyName1
     */
    public function setPropertyName1($propertyName1)
    {
        $this->propertyName1 = $propertyName1;
    }

    /**
     * @return int
     */
    public function getPropertyName2()
    {
        return $this->propertyName2;
    }

    /**
     * @param int $propertyName2
     */
    public function setPropertyName2($propertyName2)
    {
        $this->propertyName2 = $propertyName2;
    }

    /**
     * @return float
     */
    public function getPropertyName3()
    {
        return $this->propertyName3;
    }

    /**
     * @param float $propertyName3
     */
    public function setPropertyName3($propertyName3)
    {
        $this->propertyName3 = $propertyName3;
    }

    /**
     * @return array
     */
    public function getPropertyName4()
    {
        return $this->propertyName4;
    }

    /**
     * @param array $propertyName4
     */
    public function setPropertyName4($propertyName4)
    {
        $this->propertyName4 = $propertyName4;
    }

    /**
     * @return \StdClass
     */
    public function getPropertyName5()
    {
        return $this->propertyName5;
    }

    /**
     * @param \StdClass $propertyName5
     */
    public function setPropertyName5($propertyName5)
    {
        $this->propertyName5 = $propertyName5;
    }

}