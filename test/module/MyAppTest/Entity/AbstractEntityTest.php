<?php

namespace MyAppTest\Entity;

use MyAppTestAssets\Entity\ExampleEntity;

class AbstractEntityTest extends \PHPUnit_Framework_TestCase
{

    /** @var ExampleEntity */
    protected $entity;

    public function setUp()
    {
        $this->entity = new ExampleEntity();
    }

    public function tearDown()
    {
        $this->entity = null;
    }

    public function testToArrayMethodReturnsAnArrayFromDerivedClass()
    {
        $this->assertTrue(is_array($this->entity->toArray()));
    }

    public function testToArrayMethodReturnsPropertiesWithCorrectCase()
    {
        $expected = array(
            'propertyName1',
            'propertyName2',
            'propertyName3',
            'propertyName4',
            'propertyName5',
        );

        $actual = array_keys($this->entity->toArray());

        $this->assertSame($expected, $actual);
    }

    public function testToArrayMethodReturnsPropertiesWithUnmodifiedValuesAndDataTypes()
    {
        $expected = array(
            'propertyName1' => 'String',
            'propertyName2' => 10,
            'propertyName3' => 10.5,
            'propertyName4' => array('string', 10, 10.5, array('string', 10, 10.5,)),
            'propertyName5' => new \StdClass(),
        );

        $this->entity->setPropertyName1($expected['propertyName1']);
        $this->entity->setPropertyName2($expected['propertyName2']);
        $this->entity->setPropertyName3($expected['propertyName3']);
        $this->entity->setPropertyName4($expected['propertyName4']);
        $this->entity->setPropertyName5($expected['propertyName5']);

        $actual = $this->entity->toArray();

        $this->assertSame($expected, $actual);
    }

    public function testFromArrayMethodReturnsObjectWithEquivalentValues()
    {
        $data = array(
            'propertyName1' => 'String',
            'propertyName2' => 10,
            'propertyName3' => 10.5,
            'propertyName4' => array('string', 10, 10.5, array('string', 10, 10.5,)),
            'propertyName5' => new \StdClass(),
        );

        $this->entity->setPropertyName1($data['propertyName1']);
        $this->entity->setPropertyName2($data['propertyName2']);
        $this->entity->setPropertyName3($data['propertyName3']);
        $this->entity->setPropertyName4($data['propertyName4']);
        $this->entity->setPropertyName5($data['propertyName5']);

        $actual = (new ExampleEntity())->fromArray($data);

        $this->assertEquals($this->entity, $actual);
    }

    public function testFromArrayMethodThrowsExceptionWhenAnUnexpectedDataTypeIsProvided()
    {
        $unexpectedDataTypes = array(
            'String',
            10,
            10.5,
            new \StdClass(),
        );

        foreach ($unexpectedDataTypes as $unexpectedDataType) {
            $exception = false;
            try {
                (new ExampleEntity())->fromArray($unexpectedDataType);
            } catch (\Exception $e) {
                $exception = $e;
            }
            $this->assertInstanceOf('Exception', $exception);
        }
    }

}