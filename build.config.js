/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {

    php_files: {
        customCode: ['module/**'],
        customCodeTest: ['test/module/**'],
        globalConfig: ['config/**']
    }

};
