(function(){

var app = angular.module('example', []);

    app.controller('ExampleController', function($scope, $http) {
        $http.get('/alert').
            success(function(data, status, headers, config) {
                // message and data retrieved
                $scope.message = data;
            }).
            error(function(data, status, headers, config) {
                $scope.message = {
                    type: 'danger',
                    data: 'Oh dear, there was a problem....'
                };
            })
        ;

        $http.get('/example').
            success(function(data, status, headers, config) {
                $scope.todos = data;
            }).
            error(function(data, status, headers, config) {
                $scope.todos = [];
            })
        ;
    });

})();