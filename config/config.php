<?php
return array(
    'request_data_whitelist' => array(
        'document_root',
        'remote_addr',
        'remote_port',
        'server_software',
        'server_protocol',
        'server_name',
        'request_uri',
        'request_method',
    ),
    'routes' => array(
        '/' => array(
            'controller' => '\MyApp\Controller\ExampleController',
        ),
        '/alert' => array(
            'controller' => '\MyApp\Controller\AlertNotificationController',
        ),
        '/example' => array(
            'controller' => '\MyApp\Controller\ExampleController',
        ),
        '/users' => array(
            'controller' => '\MyApp\Controller\Resource\User',
        )
    ),
    'controllers' => array(
        '\MyApp\Controller\AlertNotificationController' => '\MyApp\Controller\Factory\AlertNotificationFactory',
        '\MyApp\Controller\ExampleController' => '\MyApp\Controller\Factory\ExampleFactory',
        '\MyApp\Controller\Resource\User' => '\MyApp\Controller\Resource\Factory\UserFactory'
    )
);